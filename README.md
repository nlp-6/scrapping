Elaborado por:
Honatan Alexander Palma Che
Edgar Geovani Garcia Lopez
Jorge Ramiro Ibarra Monroy


#Esta es una aplicacion en la cual se realiza un scrapeado de la pagina
#https://www.cemaco.com/ofertasdelasemana

Instrucciones
1. Crear la carpeta NLP6 en la unidad C: (C:\NLP6)
2. Copiar el codigo scrapy.py en la aplicacion Python (se puede usar visual studio code)
3. Ejecutar el codigo
4. En la ruta C:\NLP6/scrapy.json sera generado el archivo de salida

#No necesita crear un ambiente especial para su ejecucion.

El archivo de salida creado es de formato JSON.
Los features obtenidos son link (Enlace de la pagina), precioNormal (Precio normal del producto), precioRebajado (Precio con descuento del producto)

Ejemplo:
[{"link": "https://www.cemaco.com/set-de-10-paos-de-microfibra-fmq-1041949/p", "precioNormal": ["Q89.99"], "precioRebajado": ["Q62.99"]}]


Para eso debemos de ejecutar el script y este realizara el recorrido de la pagina, al finalizar generara el archivo de salida scrapy.json en el cual podemos ver los resultados del proceso.


CONTRIBUCIONES:
El codigo original lo investigó Edgar, Honatan lo moldeo con los features y al final Jorge realizoe el output del programa.

OBSERVACIONES:
La pagina de Cemaco, por seguridad no carga todo y va poco a poco por ello se paso de la pagina al html y por ello creamos los archivos htmls para que podamos barrer estos archivos y obtener de ahi la informacion de scrapy.


