###############################
#Grupo 6
#Honatan Alexander Palma Che
#Edgar Geovani Garcia Lopez
#Jorge Ramiro Ibarra Monroy
############################
import requests
from lxml import html
import json
from time import sleep

url_cemaco = 'https://www.cemaco.com/ofertasdelasemana'
response = requests.get(url_cemaco)

#print(response.text)
parser = html.fromstring(response.text)
##print(parser)
f = open("C:/NLP6/cemaco.html","w")

f.write(response.text)
f.close()

articulos = parser.xpath('//li[@layout="d6567a8f-4136-4e91-bbc1-864cd0f560f3"]//div[@class="product-item"]//div[@class="product-item__caption"]/h2/a/@href')

##print(len(articulos))
list = []
count = 0
for ar in articulos:
    artdict = {}
    link = ar
    response2 = requests.get(link)
    f = open("C:/NLP6/cemacod.html","w")
    f.write(response2.text)
    f.close()
    parser = html.fromstring(response2.text)
    titulo = parser.xpath("//h1[@class='product-detail__name']/div/text()")
    ##print(titulo)
    precioNormal = parser.xpath("//div[@class='product-detail']//div[@class='product-detail__price']//strong[@class='skuListPrice']/text()")
    ##print(precioNormal)
    precioRebajado = parser.xpath("//div[@class='product-detail']//div[@class='product-detail__price']//strong[@class='skuBestPrice']/text()")
    ##print(precioRebajado)
    detalle = parser.xpath("//div[contains (@class, 'brandName')]/a/text()")

#    print(detalle)
    titulo = parser.xpath("//h1[@id='firstHeading']/text()")
    parrafo = parser.xpath("//div[@class='mw-parser-output']/p[2]/text()")
    #artdict= {"titulo":  titulo[0], "link": link, "parrafo": ' '.join(parrafo).replace("\n","")}
    ##print(titulo)
    artdict= {"link": link,"precioNormal":  precioNormal,"precioRebajado":  precioRebajado}
 #   print(artdict)
    list.append(artdict)

    count = count + 1
    if(count > 25):
        break

jsonString = json.dumps(list)
f = open("C:/NLP6/scrapy.json","w")
f.write(jsonString)
f.close()
